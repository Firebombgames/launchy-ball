﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dungeonGenerator : MonoBehaviour
{
    public GameObject rooms;

    public GameObject startRoom;
    public List<GameObject> NorthRooms;
    public List<GameObject> SouthRooms;
    public List<GameObject> EastRooms;
    public List<GameObject> WestRooms;
    public List<GameObject> endCaps;

    public int minDistanceToExit = 6;

    public List<GameObject> needNorthExit;
    public List<GameObject> needSouthExit;
    public List<GameObject> needEastExit;
    public List<GameObject> needWestExit;
    // Start is called before the first frame update
    void Start()
    {
        rooms = GameObject.Find("Rooms");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
