﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roomSpawner : MonoBehaviour
{
    public int openingDirection;
    // B T L R
    // 1 2 3 4
    private roomTemplates templates;
    public bool spawned = false;

        
    public void Start()
    {
      //set the opening direction
       switch(transform.name)
        {
            case "SpawnN":
                openingDirection = 1;
                break;
            case "SpawnS":
                openingDirection = 2;
                break;
            case "SpawnE":
                openingDirection = 3;
                break;
            case "SpawnW":
                openingDirection = 4;
                break;
            default:
                //error
                break;
        }
        templates = GameObject.FindGameObjectWithTag("Rooms").GetComponent<roomTemplates>();
        Invoke("Spawn", 0.1f);
    }

    public void Spawn()
    {
        if (spawned == false)
        {
            //set the opening direction
            switch (transform.name)
            {
                case "SpawnN":
                    //spawn a room with a bottom(south) opening
                    Instantiate(templates.bottomRooms[Random.Range(0, templates.bottomRooms.Length)], transform.position, transform.rotation);
                    break;
                case "SpawnS":
                    //spawn a room with a top(north) opening
                    Instantiate(templates.topRooms[Random.Range(0, templates.topRooms.Length)], transform.position, transform.rotation);
                    break;
                case "SpawnE":
                    //spawn a room with a left(west) opening
                    Instantiate(templates.leftRooms[Random.Range(0, templates.leftRooms.Length)], transform.position, transform.rotation);
                    break;
                case "SpawnW":
                    //spawn a room with a right(east) opening
                    Instantiate(templates.rightRooms[Random.Range(0, templates.rightRooms.Length)], transform.position, transform.rotation);
                    break;
                default:
                    //error
                    break;
            }
            spawned = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="SpawnPoint" && other.GetComponent<roomSpawner>().spawned==true)
        {
            Destroy(gameObject);
        }
    }
}
