﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fpsTracker : MonoBehaviour
{
    public int frames = 0;
    public int fps = 0;
    public float counter = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;
        if(counter>=1)
        {
            counter -= 1;
            fps = frames;
            frames = 0;
            this.GetComponent<Text>().text = "FPS: " + fps;
        }
        else
        {
            frames++;
        }
    }
}
