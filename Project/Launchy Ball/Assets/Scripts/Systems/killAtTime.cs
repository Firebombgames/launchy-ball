﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killAtTime : MonoBehaviour
{
    public float killTime = 5f;
    public float counter = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        counter += Time.deltaTime;
        if(counter>=killTime)
        {
            Destroy(gameObject);
        }
    }
}
