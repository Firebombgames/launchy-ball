﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawner : MonoBehaviour
{
    public float maxEnemies = 200;
    public float minEnemies = 150;
    public Vector2 xRange;
    public Vector2 yRange;
    public GameObject enemyPrefab;
    // Start is called before the first frame update
    void Start()
    {
        float enemyCount = Random.Range(minEnemies - 1, maxEnemies);
        for(int i=0;i<enemyCount;i++)
        {
            Vector3 spawnPos = new Vector3(Random.Range(xRange.x, xRange.y), Random.Range(yRange.x, yRange.y), 0);
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(spawnPos,1.25f);
            if (hitColliders.Length == 0)
            {
                Instantiate(
                enemyPrefab,
                spawnPos,
                Quaternion.identity,
                GameObject.Find("_enemies_").transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
