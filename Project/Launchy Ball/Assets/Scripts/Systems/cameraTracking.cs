﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraTracking : MonoBehaviour
{
    public Transform objToTrack;

    public float maxRange;
    public float distance;

    public Vector2 speedRange;
    public Vector2 sizeRange;

    public bool zoomMode = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //get a reference to the player
        ballController b = FindObjectOfType<ballController>();

        //scale the camera based on ball speed
        if (!zoomMode)
        {
            Camera.main.orthographicSize = Mathf.Lerp(sizeRange.x, sizeRange.y, b.speed / speedRange.y);
        }
        else
        {
            Camera.main.orthographicSize = Mathf.Lerp(Camera.main.orthographicSize, sizeRange.y, Time.deltaTime);
        }

        //calculate the distance
        Vector2 cPos = transform.position;
        Vector2 pPos = b.transform.position;
        distance = Vector2.Distance(cPos, pPos);

        //move the camera to follow
        transform.position = new Vector3(
            Mathf.Lerp(transform.position.x, b.transform.position.x, Mathf.Clamp((b.speed * Time.deltaTime), 0, 1)),
            Mathf.Lerp(transform.position.y, b.transform.position.y, Mathf.Clamp((b.speed * Time.deltaTime), 0, 1)),
            -10);

    }
}
