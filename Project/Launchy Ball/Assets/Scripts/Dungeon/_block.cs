﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _block : MonoBehaviour
{
    public Transform parent;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        if(transform.parent.tag=="Walls")
        {
            parent = transform.parent.parent;
        }
        else
        {
            parent = transform.parent;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        
        if(other.transform.parent.name != parent.name)
        {
            //make sure its a different rooms collider
            if(other.tag=="RoomCenter")
            {
                Destroy(gameObject);
            }
        }
    }
}
