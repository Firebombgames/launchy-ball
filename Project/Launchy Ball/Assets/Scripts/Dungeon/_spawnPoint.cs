﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _spawnPoint : MonoBehaviour
{
    public string directionFrom = "";
    public _room previousRoom;
    public GameObject room;
    public _dungeon dungeonData;
    public GameObject spawnedRoom;
    public GameObject block;
    public int doorSize = 3;

    public void Start()
    {
        dungeonData = FindObjectOfType<_dungeon>().GetComponent<_dungeon>();
        GameObject g;
        g = Instantiate(room, previousRoom.transform.position, transform.rotation,dungeonData.transform);
        _room t=g.GetComponent<_room>();
       
        t.roomSize = new Vector2(
                (int)Random.Range(dungeonData.roomWidthRange.x, dungeonData.roomWidthRange.y),
                (int)Random.Range(dungeonData.roomHeightRange.x, dungeonData.roomHeightRange.y)); 
        if(_room.startRoom!=null)
        {
            t.setUpRoom();
        }
        if(directionFrom=="South")
        {
            //move the room up
            t.transform.Translate(0, ((t.roomSize.y/2) + (previousRoom.roomSize.y / 2)), 0);
            t.SouthExit = true;
            t.dontSpawnSouth = true;
            Debug.Log("moved room Up " + gameObject.name + "by : " + (t.roomSize.y + (previousRoom.roomSize.y / 2)));
            if(dungeonData.maxRooms>0)
            {
                //choose other exits
                //east exit
                int r = Random.Range(0, 100);
                if(r>49)
                {
                    t.EastExit = true;
                }
                //west exit
                r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.WestExit = true;
                }
                //north exit
                r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.NorthExit = true;
                }
            }
        }
        if (directionFrom == "North")
        {
            //move the room down
            t.transform.Translate(0, -((t.roomSize.y/2) + (previousRoom.roomSize.y / 2)), 0);
            t.NorthExit = true;
            t.dontSpawnNorth = true;
            Debug.Log("moved room Down " + gameObject.name + " by : " + (t.roomSize.y + (previousRoom.roomSize.y / 2)));
            if (dungeonData.maxRooms > 0)
            {
                //choose other exits
                //east exit
                int r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.EastExit = true;
                }
                //west exit
                r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.WestExit = true;
                }
                //south exit
                r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.SouthExit = true;
                }
            }
        }
        if (directionFrom == "West")
        {
            //move the room down
            t.transform.Translate(((t.roomSize.x / 2) + (previousRoom.roomSize.x / 2)), 0, 0);
            t.WestExit = true;
            t.dontSpawnWest = true;
            Debug.Log("moved room Down " + gameObject.name + " by : " + (t.roomSize.x + (previousRoom.roomSize.x / 2)));
            if (dungeonData.maxRooms > 0)
            {
                //choose other exits
                //east exit
                int r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.EastExit = true;
                }
                //North exit
                r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.NorthExit = true;
                }
                //south exit
                r = Random.Range(0, 100);
                if (r > 49)
                {
                    t.SouthExit = true;
                }
            }
        }
        spawnedRoom = g;
    }

    private void LateUpdate()
    {
        if(spawnedRoom==null)
        {
            //before the destroy it spawn blocks 
            //east and west
            if(directionFrom== "West")
            {
                float x = transform.position.x - 1;
                float y = transform.position.y - 2;
                Vector2 pos = new Vector2(x, y);
                GameObject g;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
            }
            if (directionFrom == "East")
            {
                float x = transform.position.x + 1;
                float y = transform.position.y - 2;
                Vector2 pos = new Vector2(x, y);
                GameObject g;
                g=Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.y++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
            }
            //north and south
            if (directionFrom == "North")
            {
                float x = transform.position.x - 2;
                float y = transform.position.y + 1;
                Vector2 pos = new Vector2(x, y);
                GameObject g;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
            }
            if (directionFrom == "South")
            {
                float x = transform.position.x - 2;
                float y = transform.position.y - 1;
                Vector2 pos = new Vector2(x, y);
                GameObject g;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
                pos.x++;
                g = Instantiate(block, pos, transform.rotation, transform.parent);
                g.name = name + ":FillInBlock";
            }
            //destroy the spawn point
            Destroy(gameObject);
        }
    }
}
