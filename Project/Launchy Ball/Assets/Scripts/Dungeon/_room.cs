﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _room : MonoBehaviour
{
    [Header("References")]
    public _dungeon dungeonData;
    public static GameObject startRoom;
    public Transform topWall;
    public Transform bottomWall;
    public Transform leftWall;
    public Transform rightWall;
    public BoxCollider2D roomCenter;

    [Header("Data")]
    public bool isStartRoom = false;
    public List<GameObject> blocks;
    public Vector2 roomSize;
    public bool NorthExit = false;
    public bool SouthExit = false;
    public bool EastExit = false;
    public bool WestExit = false;
    public static int roomID = 1;

    [Header("RoomSpawning")]
    public bool dontSpawnNorth = false;
    public bool dontSpawnSouth = false;
    public bool dontSpawnEast = false;
    public bool dontSpawnWest = false;

    // Start is called before the first frame update
    void Start()
    {
        name = "Room:" + roomID;
        roomID++;
        dungeonData = GameObject.FindObjectOfType<_dungeon>().GetComponent<_dungeon>();
        if (startRoom == null)
        {
            Invoke("setUpRoom", 0.2f);
            dungeonData.rooms = new List<GameObject>();
        }
        if(dungeonData.rooms.Count>=dungeonData.maxRooms && dungeonData.rooms!=null)
        {
            Destroy(gameObject);
        }
        else
        {
            dungeonData.rooms.Add(this.gameObject);
        }
    }

    // Update is called once per frame
    void spawnBlocks()
    {
        Vector2 halfSize = roomSize / 2f;
        Debug.Log("Half Size: " + halfSize);

        //GetComponent<BoxCollider2D>().size = roomSize - new Vector2(1,1);
        //topLeft
        makeBlock("topLeft", new Vector3(
            transform.position.x - (halfSize.x),
            transform.position.y + (halfSize.y),
            0));
        //topRight
        makeBlock("topRight", new Vector3(
            transform.position.x + (halfSize.x),
            transform.position.y + (halfSize.y),
            0));
        //bottomLeft
        makeBlock("bottomLeft", new Vector3(
            transform.position.x - (halfSize.x),
            transform.position.y - (halfSize.y),
            0));
        //bottomRight
        makeBlock("bottomRight", new Vector3(
           transform.position.x + (halfSize.x),
           transform.position.y - (halfSize.y),
           0));

        //fill in the rest of the blocks

        //top wall
        for(int i = 1 ; i < roomSize.x ; i++)
        {
            //check if its going to be an exit
            if(NorthExit)
            {
                if (i < halfSize.x - 2 || i > halfSize.x + 2)
                {
                    makeBlock("top" + i, new Vector3(
                                (transform.position.x - (halfSize.x)) + i,
                                transform.position.y + (halfSize.y),
                                0),
                                topWall);
                }
                else
                {
                    //create the spawn point
                    if (i == halfSize.x && dontSpawnNorth==false)
                    {
                        GameObject g = Instantiate(dungeonData.spawnPoint,
                            new Vector3(
                                transform.position.x,
                                transform.position.y + (halfSize.y) + 1,
                                0),
                            transform.rotation, transform);
                        g.GetComponent<_spawnPoint>().directionFrom = "South";
                        g.GetComponent<_spawnPoint>().previousRoom = this;
                        g.name = "SouthSpawn";
                    }
                }
            }
            else
            {
                makeBlock("top" + i, new Vector3(
                              (transform.position.x - (halfSize.x)) + i,
                              transform.position.y + (halfSize.y),
                              0),
                              topWall);
            }
           
        }
        //bottom wall
        for (int i = 1 ; i < roomSize.x ; i++)
        {
            //check if its going to be an exit
            if (SouthExit)
            {
                if (i < halfSize.x - 2 || i > halfSize.x + 2)
                {
                    makeBlock("bottom" + i, new Vector3(
                                (transform.position.x - (halfSize.x)) + i,
                                transform.position.y - (halfSize.y),
                                0),
                                bottomWall);
                }
                else
                {
                    //create the spawn point
                    if(i==halfSize.x && dontSpawnSouth == false)
                    {
                        GameObject g = Instantiate(dungeonData.spawnPoint,
                            new Vector3(
                                transform.position.x,
                                transform.position.y - (halfSize.y) - 1,
                                0),
                            transform.rotation,transform);
                        g.GetComponent<_spawnPoint>().directionFrom = "North";
                        g.GetComponent<_spawnPoint>().previousRoom = this;
                        g.name = "BottomSpawn";
                    }
                }
            }
            else
            {
                makeBlock("bottom" + i, new Vector3(
                                 (transform.position.x - (halfSize.x)) + i,
                                 transform.position.y - (halfSize.y),
                                 0),
                                 bottomWall);
            }
        }
        //right wall
        for (int i = 1 ; i < roomSize.y  ; i++)
        {
            //check if its going to be an exit
            if (EastExit)
            {
                if (i < halfSize.y - 2 || i > halfSize.y + 2)
                {
                    makeBlock("right" + i, new Vector3(
                                (transform.position.x + (halfSize.x)),
                                transform.position.y + (halfSize.y) - i,
                                0),
                                rightWall);
                }
                else
                {
                    //create the spawn point
                    if (i == halfSize.y && dontSpawnEast == false)
                    {
                        GameObject g = Instantiate(dungeonData.spawnPoint,
                            new Vector3(
                                (transform.position.x + (halfSize.x)) + 1,
                                transform.position.y,
                                0),
                            transform.rotation, transform);
                        g.GetComponent<_spawnPoint>().directionFrom = "West";
                        g.GetComponent<_spawnPoint>().previousRoom = this;
                        g.name = "Right Spawn";
                    }
                }
            }
            else
            {
                makeBlock("right" + i, new Vector3(
                               (transform.position.x + (halfSize.x)),
                               transform.position.y + (halfSize.y) - i,
                               0),
                               rightWall);
            }
        }
        //left wall
        for (int i = 1 ; i < roomSize.y; i++)
        {
            //check if its going to be an exit
            if (WestExit)
            {
                if (i < halfSize.y - 2 || i > halfSize.y + 2)
                {
                    makeBlock("left" + i, new Vector3(
                                (transform.position.x - (halfSize.x)),
                                transform.position.y + (halfSize.y) - i,
                                0),
                                leftWall);
                }
                else
                {
                    //create the spawn point
                    if (i == halfSize.y && dontSpawnEast == false)
                    {
                        GameObject g = Instantiate(dungeonData.spawnPoint,
                            new Vector3(
                                (transform.position.x - (halfSize.x)) + 1,
                                transform.position.y,
                                0),
                            transform.rotation, transform);
                        g.GetComponent<_spawnPoint>().directionFrom = "West";
                        g.GetComponent<_spawnPoint>().previousRoom = this;
                        g.name = "Left Spawn";
                    }
                }
            }
            else
            {
                makeBlock("left" + i, new Vector3(
                               (transform.position.x - (halfSize.x)),
                               transform.position.y + (halfSize.y) - i,
                               0),
                               leftWall);
            }
        }
    }

    public void setUpRoom()
    {
        //is this the start room?
        if(startRoom==null)
        {
            startRoom = this.gameObject;
            isStartRoom = true;
            NorthExit = true;
            SouthExit = true;
            EastExit = true;
            WestExit = true;
}
        //determine the start size of the room
        if (startRoom)
        {
            dungeonData = GameObject.FindObjectOfType<_dungeon>().GetComponent<_dungeon>();
            roomSize = new Vector2(
                (int)Random.Range(dungeonData.roomWidthRange.x, dungeonData.roomWidthRange.y),
                (int)Random.Range(dungeonData.roomHeightRange.x, dungeonData.roomHeightRange.y));
            if(roomSize.x % 2 != 0)
            {
                roomSize.x++;
            }
            if (roomSize.y % 2 != 0)
            {
                roomSize.y++;
            }
            roomCenter.size = roomSize - new Vector2(1.1f, 1.1f);
            GetComponent<BoxCollider2D>().size= roomSize + new Vector2(1.1f, 1.1f);
            Debug.Log("RoomSize: " + roomSize);
        }
      

        //spawn the blocks
        Invoke("spawnBlocks", 0.2f);
    }


    void makeBlock(Vector3 position)
    {
        GameObject g;
        //topLeft
        g = Instantiate(dungeonData.block, position, transform.rotation, transform);
        g.name = "block";
        blocks.Add(g);
    }

    void makeBlock(string name , Vector3 position)
    {
        GameObject g;
        //topLeft
        g = Instantiate(dungeonData.block, position, transform.rotation, transform);
        g.name = name;
        blocks.Add(g);
    }

    void makeBlock(string name, Vector3 position, Transform parent)
    {
        GameObject g;
        //topLeft
        g = Instantiate(dungeonData.block, position, transform.rotation, parent);
        g.name = name;
        blocks.Add(g);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="Rooms")
        {
            Destroy(gameObject);
        }
    }
}

