﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _dungeon : MonoBehaviour
{
    [Header ("References")]
    public GameObject roomCenter;
    public GameObject spawnPoint;
    public GameObject block;
    public List<GameObject> rooms;
    public GameObject rectangle;

    [Header("Parameters")]
    public Vector2 roomWidthRange;
    public Vector2 roomHeightRange;
    public float doorSize = 3;
    public int maxRooms = 15;
    public bool deletionComplete = false;

    public void Start()
    {
        //make sure the room is going to be big eneough to allow doors
        
        //width
        if(roomWidthRange.x < doorSize + 6)
        {
            roomWidthRange.x = doorSize + 6;
        }
        if (roomWidthRange.y < doorSize + 6)
        {
            roomWidthRange.y = doorSize + 6;
        }

        //height
        //width
        if (roomHeightRange.x < doorSize + 6)
        {
            roomHeightRange.x = doorSize + 6;
        }
        if (roomHeightRange.y < doorSize + 6)
        {
            roomHeightRange.y = doorSize + 6;
        }

    }

    public void LateUpdate()
    {
        if (rooms.Count >= maxRooms && !deletionComplete)
        {
            Invoke("cleanup", 0.35f);
            deletionComplete = true;
        }
    }

    void cleanup()
    {
        GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        GameObject[] RoomCenters = GameObject.FindGameObjectsWithTag("RoomCenter");
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            Destroy(spawnPoints[i]);
        }
        for (int i = 0; i < RoomCenters.Length; i++)
        {
            Destroy(RoomCenters[i]);
        }
        deletionComplete = true;
    }
}
