﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ballController : MonoBehaviour
{
    [Header ("References")]
    public Rigidbody2D rb;
    public LineRenderer lr;
    public PostProcessProfile defaultPostProfile;
    public PostProcessProfile slowPostProfile;
    public ParticleSystem flingParticles;

    [Header("Flinging")]
    public float flingPower = 1f;
    public bool triggerFling = false;
    public float enemyBounceForce = 100f;

    [Header("Animation")]
    public float animationSpeed = 10f;

    [Header("Values")]
    public float speed = 0;

    [Header("Tags")]
    public string enemyTag = "Enemy";

    
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //squash the ball if the mouse button is held down
        if(Input.GetMouseButton(0))
        {
            transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, 0.5f, animationSpeed * Time.deltaTime), transform.localScale.y, 1);
            //rotate to the cursor
            GetComponent<rotateToCursor>().setRotation();
            //set the time Scale
            Time.timeScale = 0.5f;
            //enable the line drawer and start drawing
            lr.enabled = true;
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, Camera.main.ScreenToWorldPoint(Input.mousePosition));
            //set the post processing profile
            Camera.main.GetComponent<PostProcessVolume>().profile = slowPostProfile;
            Camera.main.GetComponent<cameraTracking>().zoomMode = true;
        }
        else
        {
            //resume normal time speed
            Camera.main.GetComponent<cameraTracking>().zoomMode = false;
            Time.timeScale = 1;
            transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, 0.5f, animationSpeed * Time.deltaTime), transform.localScale.y, 1);

            if (speed > 0.25f)
            {
                rotateToVelocity();
                transform.localScale = new Vector3(transform.localScale.x, 0.25f, 1);
            }
            else
            {
                transform.localScale = new Vector3(transform.localScale.x, Mathf.Lerp(transform.localScale.y, 0.5f, animationSpeed * Time.deltaTime), 1);
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            triggerFling = true;
            //reset the scale
            transform.localScale = new Vector3(1, 1, 1);
            //turn off the line drawer
            lr.enabled = false;
            //reset the post processing profile
            Camera.main.GetComponent<PostProcessVolume>().profile = defaultPostProfile;
        }
    }

    private void FixedUpdate()
    {
        rb.angularVelocity = Mathf.Lerp(rb.angularVelocity, 0, Time.deltaTime * 10);
        speed=rb.velocity.magnitude;
        if(triggerFling)
        {
            //reset the players velocity
            rb.velocity = Vector2.zero;
            //fling the player towords the mouse
            rb.AddForce(angleToCursor() * (flingPower * Vector2.Distance(transform.position,Camera.main.ScreenToWorldPoint(Input.mousePosition))));
            triggerFling = false;
            //shoot out particles
            flingParticles.transform.rotation = transform.rotation;
            flingParticles.transform.Rotate(0, 0, 180);
            flingParticles.Emit(20);
        }
    }

    private void rotateToVelocity()
    {
        Vector2 v = rb.velocity;
        float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private Vector2 angleToCursor()
    {
        Vector2 a = new Vector2(
            Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x, 
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y);
        a.Normalize();
        return a;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag==enemyTag)
        {
            //it was an enemy
            collision.gameObject.GetComponent<enemyController>().kill();
            Destroy(collision.gameObject);
            rb.velocity = new Vector2(rb.velocity.x, enemyBounceForce);
        }
    }


}
