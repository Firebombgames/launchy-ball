﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateToCursor : MonoBehaviour
{
    public Vector3 mousePos;
    Vector3 objectPos;
    float angle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = Input.mousePosition;
        mousePos.z = Vector3.Distance(transform.position, Camera.main.transform.position);
        objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x = mousePos.x - objectPos.x;
        mousePos.y = mousePos.y - objectPos.y;
        angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;        
    }

    public void setRotation()
    {
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }
}
